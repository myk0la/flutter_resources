# flutter_resources

A collection of useful, interesting or just funny resources to learn flutter.

# Command Notes
```flutter build apk --realease````


# Resources
## Crash Courses

### Udemy 5-hours
[Flutter Crash Course for Beginners 2019 - Build a Flutter App with Google's Flutter & Dart](https://www.youtube.com/watch?v=x0uinJvhNxI&t=1s)
- It can be easily watched at 1.5-2.0 speed => 3 hours

#### Notes
- Everything is a widget
- UI as code

## State Management

### Provider and Scope pattern
- Great start
- Nerdy jokes :)
- [Pragmatic State Management in Flutter (Google I/O'19)](https://www.youtube.com/watch?v=d_m5csmrf7I)

#### Notes
- UI is a function of the state of the app
- If several widgets on the same (or below) sharing the same state put the state to the the next upper hierarchical element
- Provider is simmilar to observer patter but your observable is scope/state of your widget group not the widget itself
- __Do not use providers__ if you have only one widget which holds state. e.g. animations states in the widget.
- Providers support also __streams__
- It is testable
- They also used `with`keyword for mixing classes
    - [Dart for Flutter : Mixins in Dart](https://medium.com/flutter-community/https-medium-com-shubhamhackzz-dart-for-flutter-mixins-in-dart-f8bb10a3d341)
    - What is the difference between extending and with
    - Does it not leads to chaos?

### Architecture of Flutter apps
[Keep it Simple, State: Architecture for Flutter Apps (DartConf 2018)](https://www.youtube.com/watch?v=zKXz3pUkw9A)
- Tries to solve the state propagation problem
- However it is old (Jan of 2018) and uses workarounds(?)
- Good for understanding the problem of state management
- Using Redux approach

#### Notes
- You application will evolve and to be able to manage this
    - Try to structure your app using layers (hi, ISO/OSI model)
    - In this case you can update one layer but you will not have to change anything in another one
    - => Modules Encapsulation

Stateless vs Statefull
- Both can get as an input some unchangeble params like title of the screen, color of the widget, size etc.
- For statefull widget there are some params which will change such as slider value, counter value etc.
- Statefull widgets also provide methods to update its state

# Overall Notes

## SingleChildScrollView
- Its children are unlimited in width and height
- That is why is hard to create views with it
- Use __ListView__ instead
Accordingly to !(https://api.flutter.dev/flutter/widgets/SingleChildScrollView-class.html)

   When you have a list of children and do not require cross-axis shrink-wrapping behavior, for example a scrolling list that is always the width of the screen, consider ListView, which is vastly more efficient that a SingleChildScrollView containing a ListBody or Column with many children.


## Camera Plugin vs Image Picker Plugin
The two plugins differ in functionality and most importantly in purpose:

camera allows you to embed the camera feed into your own application as a widget, i.e. have control over it as well.

The image_picker plugin will launch a different application (a camera or gallery application) and return a File (an image or video file selected by the user in the other application) to your own application.

If you want to implement (and customize) the camera for you own purposes, you will have to use camera, but if you only want to retrieve image or video from the user, image_picker will be your choice.

